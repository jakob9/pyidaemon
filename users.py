import threading

import channels
import container


class InvalidNickname(Exception):
    pass


class UserContainer(container.UniqueContainer):
    pass


class ServerUserContainer(container.UniqueContainer):
    def __init__(self, *args, **kwargs):
        super(ServerUserContainer, self).__init__(*args, **kwargs)
        self._last_user_id = 0
        self._last_user_id_lock = threading.Lock()

    def _next_user_id(self):
        """
        Get the next unique user ID.

        Returns:
            (int) User ID
        """
        self._last_user_id_lock.acquire()
        try:
            self._last_user_id += 1
            return self._last_user_id
        finally:
            self._last_user_id_lock.release()

    def register(self, user):
        """
        Register a new user. Requires a nickname.
        """
        if user.userid is None:
            user.userid = self._next_user_id()
        self.add(user)

    def change_nick(self, old_nick, new_nick):
        if old_nick == new_nick:
            return

        old_nick = old_nick.lower()
        new_nick = new_nick.lower()
        self._lock.acquire()
        try:
            if old_nick == new_nick:
                # Changing cases
                self._items[new_nick] = self._items.pop(old_nick)
            else:
                # New nickname
                if new_nick in self._items.keys():
                    raise self.DublicateItem(new_nick)
                self._items[new_nick] = self._items.pop(old_nick)

        finally:
            self._lock.release()



class User(object):
    def __init__(self, conn):
        self.userid = None
        self.ident = None
        self.nick = 'IDENT'
        self.realname = None
        self.mode = ''
        self.away = False
        self.host = '127.0.0.1'
        self.channels = channels.ChannelContainer()

        # Shortcuts
        if not conn is None: # None for unit testing purposes
            self.send = conn.send

    @property
    def mask(self):
        return '%s!%s@%s' % (self.nick, self.ident, self.host)

    @property
    def name(self):
        return self.nick

    def is_identified(self):
        return not self.nick is None
        #return not None in (self.ident, self.nick)

    def in_channel(self, channel):
        return channel in self.channels

