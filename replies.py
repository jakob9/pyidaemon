

# FIXME: Move these messages to somewhere else
NUMERIC_REPLIES = {
    '001' : 'Welcome to the Internet Relay Network %s',
    '002' : 'Your host is %s, running version %s',
    '003' : 'This server was created %s',
    '004' : '<servername> <version> <available user modes> <available channel modes>',
    '318' : 'End of WHOIS list',
    '331' : 'No topic is set',
    '366' : 'End of /NAMES list.',
    '401' : 'No such nick',
    '403' : 'No such channel',
    '404' : 'Cannot send to channel',
    '405' : 'You have joined too many channels',
    '411' : 'No recipient given (%s)',
    '412' : 'No text to send',
    '421' : 'Unknown command',
    '431' : 'No nickname given',
    '432' : 'Erroneous nickname',
    '433' : 'Nickname is already in use',
    '442' : 'You\'re not on that channel',
    '451' : 'You have not registered',
    '461' : 'Not enough parameters',
    '475' : 'Cannot join channel (+k)',
}


# -- IRC REPLIES -------------------------------------------------------------

SERVER = 0
THIS_USER = 1

class IRCReply(object):
    def __init__(self, source, cmd, args=[], msg=None, msg_args=None):
        self.source = source
        self.cmd = cmd # May be a 3-digit numeric
        self.args = args
        self.msg = msg
        self.nick = None
        if self.msg is None:
            self.msg = NUMERIC_REPLIES.get(cmd)
            if self.msg and msg_args:
                self.msg = self.msg % msg_args

    def __str__(self):
        words = []
        if self.source:
            words.append(':%s' % self.source)
        words.append(self.cmd)
        if self.nick:
            words.append(self.nick)
        if self.args:
            words.append(' '.join(self.args))
        if not self.msg is None:
            words.append(':%s' % self.msg)
        return ' '.join(words)


class Welcome(IRCReply):
    """001 RPL_WELCOME"""
    def __init__(self, nick):
        IRCReply.__init__(self, SERVER, '001', msg_args=nick)


class YourHost(IRCReply):
    """002 RPL_YOURHOST"""
    def __init__(self, server, version):
        IRCReply.__init__(self, SERVER, '002', msg_args=(server, version))


class Created(IRCReply):
    """003 RPL_CREATED"""
    def __init__(self, date):
        IRCReply.__init__(self, SERVER, '003', msg_args=date)


class MyInfo(IRCReply): # FIXME
    """004 RPL_MYINFO"""
    def __init__(self):
        IRCReply.__init__(self, SERVER, '004')


class Names(IRCReply):
    """353 RPL_NAMREPLY"""
    def __init__(self, channel_name, names):
        msg = ' '.join(names)
        IRCReply.__init__(self, SERVER, '353', ['@', channel_name], msg=msg)


class EndOfNames(IRCReply):
    """366 RPL_ENDOFNAMES"""
    def __init__(self, channel_name):
        IRCReply.__init__(self, SERVER, '366', [channel_name])


class WhoIsUser(IRCReply):
    """311 RPL_WHOISUSER"""
    def __init__(self, nick, user, host, realname):
        args = [nick, user, host, '*']
        IRCReply.__init__(self, SERVER, '311', args, msg=realname)


class WhoIsServer(IRCReply):
    """312 RPL_WHOISSERVER"""
    def __init__(self, nick, server_name, server_info):
        args = [nick, server_name]
        IRCReply.__init__(self, SERVER, '312', args, msg=server_info)


class EndOfWhoIs(IRCReply):
    """318 RPL_ENDOFWHOIS"""
    def __init__(self, nick):
        IRCReply.__init__(self, SERVER, '318', [nick])


class WhoIsChannels(IRCReply):
    """319 RPL_WHOISCHANNELS"""
    def __init__(self, nick, channels):
        if isinstance(channels, list):
            channels = ' '.join(channels)
        IRCReply.__init__(self, SERVER, '319', [nick], msg=channels)


class Ping(IRCReply):
    def __init__(self, server):
        IRCReply.__init__(self, SERVER, 'PING', [server])


class Pong(IRCReply):
    def __init__(self, server):
        IRCReply.__init__(self, SERVER, 'PONG', [server])


class Nick(IRCReply):
    def __init__(self, user, new_nick):
        IRCReply.__init__(self, user.mask, 'NICK', [new_nick])


class Join(IRCReply):
    def __init__(self, user, channel_name):
        IRCReply.__init__(self, user.mask, 'JOIN', [channel_name])


class Part(IRCReply):
    def __init__(self, user, channel_name, msg=None):
        IRCReply.__init__(self, user.mask, 'PART', [channel_name], msg=msg)


class Topic(IRCReply):
    def __init__(self, channel_name, topic):
        IRCReply.__init__(self, SERVER, '332', [channel_name], msg=topic)


class TopicSet(IRCReply):
    def __init__(self, user, channel_name, topic):
        IRCReply.__init__(self, user.mask, 'TOPIC', [channel_name], msg=topic)


class NoTopic(IRCReply):
    def __init__(self, channel_name):
        IRCReply.__init__(self, SERVER, '331', [channel_name])


class PrivMsg(IRCReply):
    def __init__(self, from_user, target, msg):
        IRCReply.__init__(self, from_user.mask, 'PRIVMSG', [target], msg=msg)


class Notice(IRCReply):
    def __init__(self, from_user, target, msg):
        IRCReply.__init__(self, from_user.mask, 'NOTICE', [target], msg=msg)


# -- IRC ERRORS --------------------------------------------------------------

class IRCError(IRCReply, Exception):
    def __init__(self, *args, **kwargs):
        IRCReply.__init__(self, SERVER, *args, **kwargs)


class NoSuchNick(IRCError):
    """401 ERR_NOSUCHNICK"""
    def __init__(self, nick):
        IRCError.__init__(self, '401', [nick])


class NoSuchChannel(IRCError):
    """403 ERR_NOSUCHCHANNEL"""
    def __init__(self, channel_name):
        IRCError.__init__(self, '403', [channel_name])


class CanNotSendToChan(IRCError):
    """404 ERR_CANNOTSENDTOCHAN"""
    def __init__(self, channel_name):
        IRCError.__init__(self, '404', [channel_name])


class TooManyChannels(IRCError):
    """405 ERR_TOOMANYCHANNELS"""
    def __init__(self, channel_name):
        IRCError.__init__(self, '405', [channel_name])


class NoRecipient(IRCError):
    """411 ERR_NORECIPIENT"""
    def __init__(self, cmd):
        IRCError.__init__(self, '411', msg_args=cmd.upper())


class NoTextToSend(IRCError):
    """412 ERR_NOTEXTTOSEND"""
    def __init__(self):
        IRCError.__init__(self, '412')


class UnknownCommand(IRCError):
    """421 ERR_UNKNOWNCOMMAND"""
    def __init__(self, cmd):
        IRCError.__init__(self, '421', [cmd.upper()])


class NoNicknameGiven(IRCError):
    """431 ERR_NONICKNAMEGIVEN"""
    def __init__(self):
        IRCError.__init__(self, '431')


class ErroneousNickname(IRCError):
    """432 ERR_ERRONEUSNICKNAME"""
    def __init__(self, nick):
        IRCError.__init__(self, '432', [nick])


class NicknameInUse(IRCError):
    """433 ERR_NICKNAMEINUSE"""
    def __init__(self, nick):
        IRCError.__init__(self, '433', [nick])


class NotOnChannel(IRCError):
    """442 ERR_NOTONCHANNEL"""
    def __init__(self, channel_name):
        IRCError.__init__(self, '442', [channel_name])


class NotRegistered(IRCError):
    """451 ERR_NOTREGISTERED"""
    def __init__(self):
        IRCError.__init__(self, '451')


class NeedMoreParams(IRCError):
    """461 ERR_NEEDMOREPARAMS"""
    def __init__(self, cmd):
        IRCError.__init__(self, '461', [cmd.upper()])


class BadChannelKey(IRCError):
    """475 ERR_BADCHANNELKEY"""
    def __init__(self, channel_name):
        IRCError.__init__(self, '475', [channel_name])


if __name__ == '__main__':
    reply = YourHost('server', 'version')
    print str(reply)

    try:
        raise BadChannelKey('#chan')
    except IRCError, e:
        print str(e)
