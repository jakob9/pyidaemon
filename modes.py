import threading


class InvalidMode(Exception):
    pass


class Modes(object):

    valid_modes = tuple()
    default_modes = tuple()

    def __init__(self):
        self._modes = set()
        self._lock = threading.Lock()
        self.add(self.default_modes)

    def __str__(self):
        self._lock.acquire()
        try:
            return ''.join(self._modes)
        finally:
            self._lock.release()

    def __contains__(self, modes):
        self._lock.acquire()
        try:
            for mode in modes:
                if not mode in self._modes:
                    return False
            return True
        finally:
            self._lock.release()

    def __iter__(self):
        """
        Iterate over a mode flags.
        """
        self._lock.acquire()
        try:
            return iter(list(self._modes))
        finally:
            self._lock.release()

    def add(self, modes):
        """
        Add mode(s).

        Args:
            modes: (str) Mode flags to add. The string can contain more
                than one flag. No seperator is needed.

        Raises:
            (InvalidMode) Raised when trying to add an invalid mode flag.
        """
        self._lock.acquire()
        try:
            for mode in modes:
                if not mode in self.valid_modes:
                    raise InvalidMode(mode)
                self._modes.add(mode)
        finally:
            self._lock.release()

    def remove(self, modes):
        """
        Remove mode(s).

        Args:
            modes: (str) Mode flags to remove. The string can contain more
                than one flag. No seperator is needed.
        """
        self._lock.acquire()
        try:
            for mode in modes:
                try:
                    self._modes.remove(mode)
                except KeyError:
                    pass
        finally:
            self._lock.release()


class ChannelModes(Modes):
    """
    a - anonymous channel flag
    i - invite-only channel flag
    m - moderated channel
    n - no messages to channel from clients on the outside
    q - quiet channel flag
    p - private channel flag
    s - secret channel flag
    r - server reop channel flag
    t - topic settable by channel operator only flag
    b - ban mask to keep users out
    e - an exception mask to override a ban mask
    I - an invitation mask to automatically override the invite-only flag
    """
    valid_modes = (
        'a', 'i', 'm', 'n', 'q', 'p',
        's', 'r', 't', 'b', 'e', 'I',
    )
    default_modes = ('t', 'n')


class ChannelUserModes(Modes):
    """
    O - "channel creator" status
    o - channel operator privilege
    v - voice privilege
    """
    valid_modes = ('O', 'o', 'v')


class UserModes(Modes):
    """
    a - user is flagged as away
    i - marks a users as invisible
    w - user receives wallops
    r - restricted user connection
    o - operator flag
    O - local operator flag
    s - marks a user for receipt of server notices
    """
    valid_modes = ('a', 'i', 'w', 'r', 'o', 'O', 's')


if __name__ == '__main__':
    print 'Unit testing of Modes object'

    m = UserModes()
    m.add('a')
    assert ('a' in m)
    m.add('io')
    assert ('i' in m)
    assert ('o' in m)
    assert ('io' in m)
    assert (not 'iox' in m)
    m.remove('x') # non-existing mode
    m.remove('o')
    assert (not 'o' in m)
    assert (str(m) == 'ai')
    assert (list(m) == ['a', 'i'])

    try:
        m.add('x')
    except InvalidMode:
        pass
    else:
        raise AssertionError

    print 'WOW much correct'


