import threading
import modes

import users
import container



class ChannelContainer(container.UniqueContainer):
    """
    Container for storing multiple channels.
    """
    pass


class ServerChannelContainer(container.UniqueContainer):
    """
    Container for storing multiple channels (SERVER VERSION ONLY).
    Only server is allowed to create channels. Also, some methods are only
    appropriate to use when managing channels on the server.
    This class should only be implemented by the server itself.
    """

    def remove_if_empty(self, channel):
        """
        Removes a channel only if it is empty.

        Args:
            channel: (Channel) Channel-object to remove

        Returns:
            (bool) Whether or not the channel was removed
        """
        name_lower = channel.name.lower()
        self._lock.acquire()
        try:
            if name_lower in self._items.keys():
                if len(channel) == 0:
                    self._items.pop(name_lower)
                    return True
                else:
                    return False
        finally:
            self._lock.release()

    def create(self, name):
        """
        Creates a new channel.

        Args:
            name: (str) Name of channel

        Returns:
            (Channel) The new channel

        Raises:
            (ChannelAlreadyExists) Raised if a channel by that 
                name already exists
        """
        channel = Channel(name)
        name_lower = name.lower()
        self._lock.acquire()
        try:
            if name_lower in self._items.keys():
                raise self.DublicateItem(name)
            self._items[name_lower] = channel
            return channel
        finally:
            self._lock.release()

    def get_create(self, name):
        """
        Returns a channel from the server. Creates if if it does not exist.

        Args:
            name: (str) Name of channel

        Returns:
            (Channel) Requested channel
        """
        name_lower = name.lower()
        self._lock.acquire()
        try:
            if name_lower in self._items.keys():
                return self._items[name_lower]
            else:
                channel = Channel(name)
                self._items[name_lower] = channel
                return channel
        finally:
            self._lock.release()



class ChannelDoesNotExist(Exception):
    pass


class ChannelAlreadyExists(Exception):
    pass


class InvalidChannelName(Exception):
    pass


class UserNotInChannel(Exception):
    pass


class UserAlreadyInChannel(Exception):
    pass


class Channel(object):
    """
    High level abstraction of an IRC channel. This class does not
    implement aspects of the IRC protocol.
    """

    def __init__(self, name):
        self._name = None
        self.name = name # property
        self.modes = modes.ChannelModes()
        self.key = None
        self.topic = "HEY"

        # User-objects
        self._users = set()
        self._users_lock = threading.Lock()

        # User modes (userid -> ChannelUserModes-object)
        self._users_modes = {}
        self._users_modes_lock = threading.Lock()

    def __len__(self):
        self._users_lock.acquire()
        try:
            return len(self._users)
        finally:
            self._users_lock.release()

    def __contains__(self, user):
        return self.has_user(user)

    def __iter__(self):
        """
        Iterate over a list of User-objects.
        """
        self._users_lock.acquire()
        try:
            return iter(list(self._users))
        finally:
            self._users_lock.release()
        
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        if not name.startswith('#'):
            raise InvalidChannelName(name)
        self._name = name

    def is_op(self, user):
        return self.user_has_modes(user, 'o')

    def set_op(self, user):
        self.user_add_modes(user, 'o')

    def set_deop(self, user):
        self.user_remove_modes(user, 'o')

    def is_voice(self, user):
        return self.user_has_modes(user, 'v')

    def set_voice(self, user):
        self.user_add_modes(user, 'v')

    def set_devoice(self, user):
        self.user_remove_modes(user, 'v')

    def user_add_modes(self, user, flags):
        """
        Add mode(s) to a user.

        Args:
            user: (User) User to add modes to
            flags: (str) Mode flag(s) to add

        Raises:
            (UserNotInChannel) Raised if user was not found in channel
        """
        self._users_modes_lock.acquire()
        try:
            if not user.userid in self._users_modes.keys():
                raise UserNotInChannel(user.nick)
            self._users_modes[user.userid].add(flags)
        finally:
            self._users_modes_lock.release()

    def user_remove_modes(self, user, flags):
        """
        Remove mode(s) from a user.

        Args:
            user: (User) User to remove modes from
            flags: (str) Mode flag(s) to remove

        Raises:
            (UserNotInChannel) Raised if user was not found in channel
        """
        self._users_modes_lock.acquire()
        try:
            if not user.userid in self._users_modes.keys():
                raise UserNotInChannel(user.nick)
            self._users_modes[user.userid].remove(flags)
        finally:
            self._users_modes_lock.release()

    def user_has_modes(self, user, flags):
        """
        Check whether or not a user has certain modes.

        Args:
            user: (User) User to check modes for
            flags: (str) Mode flag(s) to check for
            
        Raises:
            (UserNotInChannel) Raised if user was not found in channel
        """
        self._users_modes_lock.acquire()
        try:
            if not user.userid in self._users_modes.keys():
                raise UserNotInChannel(user.nick)
            return flags in self._users_modes[user.userid]
        finally:
            self._users_modes_lock.release()

    def has_user(self, user):
        """
        Check if a user is present in the channel.

        Args:
            user: (User) User to check for

        Returns:
            (bool) True if the user is present in the channel, False otherwise
        """
        self._users_lock.acquire()
        try:
            return user in self._users
        finally:
            self._users_lock.release()

    def add_user(self, user):
        """
        Add a user to the channel.

        Args:
            user: (User) User to add

        Raises:
            (UserAlreadyInChannel) Raised if user is already in channel
        """
        self._users_modes_lock.acquire()
        self._users_lock.acquire()
        try:
            if user in self._users:
                raise UserAlreadyInChannel(user.nick)
            self._users.add(user)
            self._users_modes[user.userid] = modes.ChannelUserModes()
        finally:
            self._users_modes_lock.release()
            self._users_lock.release()

    def remove_user(self, user):
        """
        Remove user from the channel.

        Args:
            user: (User) User to remove

        Raises:
            (UserNotInChannel) Raised if user was not found in channel
        """
        self._users_modes_lock.acquire()
        self._users_lock.acquire()
        try:
            if not user in self._users:
                raise UserNotInChannel(user.nick)

            self._users.remove(user)
            self._users_modes.pop(user.userid)
        finally:
            self._users_modes_lock.release()
            self._users_lock.release()

    def send_all(self, *args, **kwargs):
        return self.invoke_all('send', *args, **kwargs)

    def invoke_all(self, method_name, *args, **kwargs):
        """
        Invoke method on all users in channel.

        Args:
            method_name: (str) Name of method to invoke
            exclude: ([User]) User-object, or list of User-objects
                to exclude

        Returns:
            ([User]) List of users who was invoked
        """
        exclude = []
        invoked_on = []
        
        if kwargs.has_key('exclude'):
            exclude = kwargs.pop('exclude')
            if not isinstance(exclude, list):
                exclude = [exclude]
                
        for user in self:
            if not user in exclude:
                method = getattr(user, method_name)
                method(*args, **kwargs)
                invoked_on.append(user)

        return invoked_on


if __name__ == '__main__':
    print 'Unit testing of Channel object'

    user1 = users.User(None, 1)
    user1.nick = 'Jakob'
    user2 = users.User(None, 2)
    user2.nick = 'John'

    channel1 = Channel('#channel1')
    print channel1.name

    channel1.add_user(user1)
    channel1.add_user(user2)
    assert (len(channel1) == 2)
    assert (user1 in channel1)
    assert (user2 in channel1)

    channel1.remove_user(user2)
    assert (len(channel1) == 1)
    assert (user2 not in channel1)

    channel1.user_add_modes(user1, 'o')
    assert (channel1.user_has_modes(user1, 'o'))
    assert (not channel1.user_has_modes(user1, 'v'))
    channel1.user_add_modes(user1, 'v')
    assert (channel1.user_has_modes(user1, 'ov'))

    channel1.user_remove_modes(user1, 'o')
    assert (not channel1.user_has_modes(user1, 'o'))

    # ------------------------------------------------------------------------

    print 'Unit testing ChannelContainer object'

    container = ServerChannelContainer()
    
    container.add(channel1)
    channel2 = container.create('#channel2')
    assert (len(container) == 2)
    assert (channel1 in container)
    assert (channel2 in container)
    assert (container.get('#channel1') == channel1)
    assert (container.get('#channel2') != channel1)
    assert (container.get_create('#channel1') == channel1)
    assert (container.get_create('#channel2') != channel1)
    
    channel3 = container.get_create('#channel3')
    assert (container.has('#channel3'))
    assert (container.get('#channel3') == channel3)

    try:
        container.get('#NonExistingChannelName')
    except container.ItemDoesNotExists:
        pass
    else:
        raise AssertionError

    container.remove(channel2)
    assert (len(container) == 2)
    assert (not channel2 in container)
    
    assert (container.remove_if_empty(channel1) == False)
    channel1.remove_user(user1)
    assert (container.remove_if_empty(channel1) == True)

    print 'WOW much correct'
