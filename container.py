import threading


class UniqueContainer(object):
    """
    Implements a container for storing unique objects.

    Objects are characterized by:
    - They have a 'name' attribute
    - Names are unique (case insensitive)
    """

    class DublicateKey(Exception):
        pass

    class DublicateItem(Exception):
        pass

    class ItemDoesNotExists(Exception):
        pass


    def __init__(self):
        self._items = {}
        self._lock = threading.Lock()

    def __len__(self):
        self._lock.acquire()
        try:
            return len(self._items)
        finally:
            self._lock.release()

    def __contains__(self, obj):
        return self.has(obj)

    def __iter__(self):
        """
        Iterate over a list of Channel-objects.

        Returns:
            An iterable copy of channels in container.
        """
        self._lock.acquire()
        try:
            return iter(list(self._items.values()))
        finally:
            self._lock.release()

    def has(self, obj):
        """
        Check whether a channel is in the container.

        Args:
            channel: (Channel/str) Channel-object OR name of channel

        Returns:
            (bool): True if channel exists, False otherwise
        """
        if isinstance(obj, str):
            name_lower = obj.lower()
        else:
            name_lower = obj.name.lower()

        self._lock.acquire()
        try:
            return name_lower in self._items.keys()
        finally:
            self._lock.release()

    def add(self, obj):
        """
        Add a channel to the container.

        Args:
            channel: (Channel) Channel to add
        """
        name_lower = obj.name.lower()
        self._lock.acquire()
        try:
            if name_lower in self._items.keys():
                raise self.DublicateKey(obj.name)
            if obj in self._items.values():
                raise self.DublicateItem(obj.name)
            self._items[name_lower] = obj
        finally:
            self._lock.release()

    def remove(self, obj):
        """
        Remove a channel to the container.

        Args:
            channel: (Channel) Channel-object to create
        """
        name_lower = obj.name.lower()
        self._lock.acquire()
        try:
            if name_lower in self._items.keys():
                self._items.pop(name_lower)
        finally:
            self._lock.release()

    def get(self, name):
        """
        Retrieve a channel from the server.

        Args:
            name: (str) Name of channel to get

        Return:
            (Channel) Requested channel

        Raises:
            (KeyError) Raised if the channel does not exists
        """
        name_lower = name.lower()
        self._lock.acquire()
        try:
            if not name_lower in self._items.keys():
                raise self.ItemDoesNotExists(name)
            return self._items[name_lower]
        finally:
            self._lock.release()
