import re
import threading
import SocketServer

import replies
import users
import channels


SERVER_NAME = 'localhost'

MSG_MAX_LENGTH = 510 # 512 - 2 (\r\n)
NICK_MAX_LENGTH = 15
USER_MAX_CHANNELS = 15


class IRCServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    def __init__(self, *args, **kwargs):
        self.name = SERVER_NAME
        self.info = 'PWNER SERVER!'
        self.channels = channels.ServerChannelContainer()
        self.users = users.ServerUserContainer()
        SocketServer.TCPServer.__init__(self, *args, **kwargs)

    @property
    def source(self):
        return self.name


class Connection(SocketServer.BaseRequestHandler):
    _linesep_regex = re.compile(r'\r?\n')
    _wordsep_regex = re.compile(r'\s')
    channel_name_regex = re.compile(r'^[&#+!]?([^,\s]{1,50})$')

    def setup(self):
        self.user = users.User(self)
        self.registered = False

    def handle(self):
        buffer = ''

        # FIXME: Handle buffer overflow

        while 1:
            data = self.request.recv(1024)
            if not data:
                continue

            buffer += data
            lines = self._linesep_regex.split(buffer)
            buffer = lines[-1]

            for line in lines[:-1]:
                if not line.strip():
                    continue

                print 'RECV: %s' % line
                try:
                    self._handle_line(line)
                except replies.IRCError, e:
                    self.send(e)

    def send(self, reply):
        if isinstance(reply, replies.IRCReply):
            if reply.source == replies.SERVER:
                reply.source = self.server.source
                reply.nick = self.user.nick
            elif reply.source == replies.THIS_USER:
                reply.source = self.user.mask
            reply = str(reply)
        if len(reply) > 510:
            raise RuntimeError('Sending too much data (max. length is 510)')
        print 'SEND: %s' % reply
        print
        self.request.send('%s\r\n' % reply)

    def _handle_line(self, line):
        cmd, args, msg = self._parse_line(line)
        print 'cmd: %s' % cmd
        print 'args: %s' % args
        print 'msg: %s' % msg
        print

        # Handle user registration if required
        if not self.registered:
            self._handle_registration(cmd, args, msg)
            return

        # Invoke command handler if it exists
        handler = self._get_handler(cmd)
        if not handler is None:
            handler(args, msg)
        else:
            self.send(replies.UnknownCommand(cmd))
    
    def _parse_line(self, line):
        """
        Invoked whenever the server recieves a new line from the client.
        According to the RFC clients SHOULD NOT send prefixes, so these
        are not handled. Method assumes that line contains characters
        other than spaces.

        Parses lines in the following format:
            command [arg1 [arg2 ...]] [:message]

        Returns:
            (command, [args], msg)

            command: (str) UPPER-case command
            args: (list) a list of args (strings). List may be empty.
            msg: (str) The text folling the colon. May be an empty string.
                If the line does not contain a colon, msg is None.
        """
        words = self._wordsep_regex.split(line)
        cmd = words[0].upper()
        args = []
        msg = []
        append = False

        for word in words[1:]:
            if append:
                msg.append(word)
            elif word and word[0] == ':':
                append = True
                msg.append(word[1:])
            elif word:
                args.append(word)

        msg = ' '.join(msg) if msg else None
        return cmd, args, msg

    def _get_handler(self, cmd):
        """
        Returns handler method for command cmd.
        cmd must be upper-case letters.
        Handlers are named as "on_CMD".
        """
        handler_name = 'on_%s' % cmd
        if hasattr(self, handler_name):
            handler = getattr(self, handler_name)
            if callable(handler):
                return handler
        return None

    def _invoke_handler(self, cmd, args, msg):
        """
        Invoke command handler if it exists.
        """
        handler = self._get_handler(cmd)
        if not handler is None:
            handler(args, msg)

    def _handle_registration(self, cmd, args, msg):
        if not self.user.is_identified():
            if cmd in ('NICK', 'USER', 'PASS', 'SERVICE', 'CAP'):
                self._invoke_handler(cmd, args, msg)
                return
            else:
                raise replies.NotRegistered()

        try:
            self.server.users.register(self.user)
        except self.server.users.DublicateKey, e:
            raise replies.NicknameInUse(str(e))

        self.registered = True
        self.send(replies.Welcome(self.user.nick))
        self.send(replies.YourHost(self.server.name, '0.1')) # FIXME
        self.send(replies.Created('I DAG!')) # FIXME
        self.send(replies.MyInfo()) # FIXME


    # -- COMMAND HANDLERS ----------------------------------------------------

    def on_PING(self, args, msg):
        """
        Syntax:
            PING <server1> [<server2>]

        Tests the presence of a connection. 
        A PING message results in a PONG reply
        """
        self.send(replies.Pong(self.server.name))

    def on_MODE(self, args, msg):
        """
        Syntax:
            MODE <nickname> <flags> (user)
            MODE <channel> [<flags>] [<args>]

        The MODE command is dual-purpose. It can be used to set 
        both user and channel modes.
        """
        if len(args) == 0:
            raise replies.NeedMoreParams('MODE')

        # Target may be either a channel or a user
        target = args[0]
        
        if target[0] == '#':
            # Channel modes
            try:
                channel = self.server.channels.get(target)
            except self.server.channels.ItemDoesNotExists:
                raise replies.NoSuchChannel(target)

            if not self.user in channel:
                raise replies.NotOnChannel(target)

            if len(args) == 1:
                # Requesting channel modes
                pass

            elif len(args) == 2:
                # Setting channel modes
                pass

        else:
            # User modes
            pass

    def on_NICK(self, args, msg):
        """
        Syntax:
            NICK :<nickname>

        Allows a client to change their IRC nickname.
        """
        if len(args) == 0 and not msg:
            raise replies.NoNicknameGiven()

        # Apparently mIRC will send nick as either arg or message?!
        if not msg is None:
            nick = msg[:NICK_MAX_LENGTH].split()[0]
        else:
            nick = args[0][:NICK_MAX_LENGTH]

        # FIXME: 432 ERR_ERRONEUSNICKNAME (min length + invalid chars)

        if nick == self.user.nick:
            return

        if self.registered:
            try:
                self.server.users.change_nick(self.user.nick, nick)
            except self.server.users.DublicateItem:
                raise replies.NicknameInUse(nick)

            # Nick change complete - announce to channels
            notified_users = [self.user]
            for channel in self.user.channels:
                n = channel.send_all(replies.Nick(self.user, nick),
                                     exclude=notified_users)
                notified_users.extend(n)
        
            self.send(replies.Nick(self.user, nick))

        self.user.nick = nick

    def on_USER(self, args, msg):
        self.user.ident = args[0]
        self.user.ident = args[1]
        self.user.realname = msg

    def on_JOIN(self, args, msg):
        """
        Syntax:
            JOIN <channels> [<keys>]

        Makes the client join the channels in the comma-separated list <channels>, 
        specifying the passwords, if needed, in the comma-separated list <keys>.
        If the channel(s) do not exist then they will be created.
        """
        if len(args) == 0:
            raise replies.NeedMoreParams('JOIN')

        if len(args) == 1 and args[0] == '0':
            # Special case where the only argument is "0" is an
            # indication by the user to leave all the channels he is on
            pass

        # FIXME: ERR_INVITEONLYCHAN
        # FIXME: ERR_CHANNELISFULL
        
        channels = args[0].split(',')
        if len(args) > 1:
            keys = args[1].split(',')
        else:
            keys = []
        
        for i, channel_name in enumerate(channels):
            if len(self.user.channels) == USER_MAX_CHANNELS:
                raise replies.TooManyChannels(channel_name)

            # Pair with key?
            if len(keys) >= (i + 1):
                key = keys[i] if keys[i] else None
            else:
                key = None

            # Validate (and normalize) channel name
            m = re.match(self.channel_name_regex, channel_name)
            if m is None:
                self.send(replies.NoSuchChannel(channel_name))
                continue

            # Get channel if exists, or create if it doesn't
            channel_name = '#%s' % m.groups()[0]
            channel = self.server.channels.get_create(channel_name)

            # Validate key (if required)
            if channel.key and key != channel.key:
                self.send(replies.BadChannelKey(channel.name))
                continue
            
            # Ready to join channel
            try:
                self.user.channels.add(channel)
            except self.user.channels.DublicateItem:
                continue
            channel.add_user(self.user)
            channel.send_all(replies.Join(self.user, channel.name))

            # Send topic
            if channel.topic:
                self.send(replies.Topic(channel.name, channel.topic))
            else:
                self.send(replies.NoTopic(channel.name))

            # Send names (including self)
            self.send_names(channel)

    def on_PART(self, args, msg):
        """
        Syntax:
            PART <channels> [:<message>]

        Causes a user to leave the channels in the comma-separated list.
        """
        if len(args) == 0:
            raise replies.NeedMoreParams('PART')
        
        channel_names = args[0].split(',')

        for channel_name in channel_names:
            try:
                channel = self.server.channels.get(channel_name)
            except self.server.channels.ItemDoesNotExists:
                self.send(replies.NoSuchChannel(channel_name))
                continue

            try:
                self.user.channels.remove(channel)
                channel.remove_user(self.user)
            except channels.UserNotInChannel:
                self.send(replies.NotOnChannel(channel.name))
                continue
            
            self.user.send(replies.Part(self.user, channel.name, msg))

            removed = self.server.channels.remove_if_empty(channel)
            if not removed:
                channel.send_all(replies.Part(self.user, channel.name, msg), 
                                 exclude=self.user)

    def on_TOPIC(self, args, msg):
        """
        Syntax:
            TOPIC <channel> [:<topic>]

        Allows the client to query or set the channel topic on <channel>.
        If <topic> is given, it sets the channel topic to <topic>. 
        If channel mode +t is set, only a channel operator may set the topic.
        """
        if len(args) == 0:
            raise replies.NeedMoreParams('TOPIC')

        channel_name = args[0]

        try:
            channel = self.server.channels.get(channel_name)
        except self.server.channels.ItemDoesNotExists:
            raise replies.NotOnChannel(channel_name)

        if not self.user in channel:
            raise replies.NotOnChannel(channel_name)

        if msg is None:
            # Requesting topic
            if channel.topic:
                self.send(replies.Topic(channel.name, channel.topic))
            else:
                self.send(replies.NoTopic(channel.name))
        else:
            # Setting topic
            # FIXME: Only ops can set topic
            # FIXME: Max length
            channel.topic = msg

            reply = replies.TopicSet(self.user, channel.name, channel.topic)
            channel.send_all(reply)

    def on_PRIVMSG(self, args, msg):
        """
        Syntax:
            PRIVMSG <msgtarget> :<message>

        Sends <message> to <msgtarget>, which can be a user or channel.
        """
        if len(args) == 0:
            raise replies.NoRecipient('PRIVMSG')
        if not msg:
            raise replies.NoTextToSend()

        # FIXME: Multiple recipients
        # FIXME: Parse recipients from masks
        recipient = args[0]

        if recipient[0] == '#':
            # Message to channel
            try:
                channel = self.server.channels.get(recipient)
            except self.server.channels.ItemDoesNotExists:
                raise replies.CanNotSendToChan(recipient)
            
            # FIXME: Only validate if user is in channel if n-flag is set
            if not self.user in channel:
                raise replies.CanNotSendToChan(channel.name)

            reply = replies.PrivMsg(self.user, channel.name, msg)
            channel.send_all(reply, exclude=self.user)
        else:
            # Message to user
            try:
                user = self.server.users.get(recipient)
            except self.server.users.ItemDoesNotExists:
                raise replies.NoSuchNick(recipient)

            # FIXME: User is away
            user.send(replies.PrivMsg(self.user, user.nick, msg))

    def on_NOTICE(self, args, msg):
        """
        Syntax:
            NOTICE <msgtarget> <message>

        This command works similarly to PRIVMSG, except automatic 
        replies must never be sent in reply to NOTICE messages.
        In other words, NOTICE fails silently.
        """
        if len(args) == 0:
            return
        if not msg:
            return

        # FIXME: Multiple recipients
        # FIXME: Parse recipients from masks
        recipient = args[0]

        if recipient[0] == '#':
            # Message to channel
            try:
                channel = self.server.channels.get(recipient)
            except self.server.channels.ItemDoesNotExists:
                return
            
            # FIXME: Only validate if user is in channel if n-flag is set
            if not self.user in channel:
                return

            reply = replies.Notice(self.user, channel.name, msg)
            channel.send_all(reply, exclude=self.user)
        else:
            # Message to user
            try:
                user = self.server.users.get(recipient)
            except self.server.users.ItemDoesNotExists:
                return

            # FIXME: User is away
            user.send(replies.Notice(self.user, user.nick, msg))

    def on_WHOIS(self, args, msg):
        """
        Syntax:
            WHOIS [<server>] <nicknames>

        Returns information about the comma-separated list of nicknames
        masks <nicknames>. If <server> is given, the command is forwarded 
        to it for processing (NOT SUPPORTED).
        """
        if len(args) == 0:
            raise replies.NoNicknameGiven()
        elif len(args) == 1:
            nick = args[0]
        elif len(args) > 1:
            nick = args[1]

        try:
            user = self.server.users.get(nick)
        except self.server.users.ItemDoesNotExists:
            self.send(replies.NoSuchNick(nick))
            self.send(replies.EndOfWhoIs(nick))
        else:
            self.send(replies.WhoIsUser(user.nick, 
                                        user.ident,
                                        user.host, 
                                        user.realname))
            channels = [c.name for c in list(user.channels)]
            if channels:
                self.send(replies.WhoIsChannels(user.nick, channels))
            self.send(replies.WhoIsServer(user.nick, 
                                          self.server.name, 
                                          self.server.info))
            self.send(replies.EndOfWhoIs(user.nick))

    def send_names(self, channel):
        """
        Send list of names on channel
        """
        names = [user.nick for user in channel]
        self.send(replies.Names(channel.name, names))
        self.send(replies.EndOfNames(channel.name))



if __name__ == '__main__':
    ip = '127.0.0.1'
    port = 5555

    server = IRCServer((ip, port), Connection)
    server.serve_forever()
